"""
Author: Simon Vandevelde
"""
import sys
import time
from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow, QAction,
                             QTabWidget, QVBoxLayout, QPushButton,
                             QInputDialog, QLineEdit, QLabel, QTableWidget,
                             QGridLayout, QTableWidgetItem,
                             QAbstractScrollArea, QComboBox, QHBoxLayout,
                             QCompleter, QSizePolicy, QFrame, QCheckBox,
                             QRadioButton, QSpacerItem, QProgressBar)
from PyQt5.QtGui import (QIcon, QIntValidator, QFont, QColor, QPixmap, QPen,
                         QBrush, QPainter, QPalette)
from PyQt5.QtCore import pyqtSlot, Qt, QThread, pyqtSignal


class controller():
    def __init__(self):
        app = QApplication(sys.argv)
        self.ex = main_window(self)
        sys.exit(app.exec_())

    def download(self):
        info_widget = self.ex.main_widget.info_widget
        le = self.ex.main_widget.le_url
        url = self.ex.main_widget.le_url.text()

        self.dl = Downloader(url)

        self.dl.scanning.connect(info_widget.set_scanning)
        self.dl.total.connect(info_widget.set_progress)
        self.dl.update.connect(info_widget.update_progress)
        self.dl.incorrect_input.connect(le.activate_wrong_input)
        self.dl.incorrect_input.connect(info_widget.set_wrong_input)
        self.dl.correct_input.connect(le.deactivate_wrong_input)
        self.dl.start()



class main_window(QMainWindow):
    def __init__(self, controller):
        super().__init__()
        self.c = controller
        self.title = 'Youtube Downloader'
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.left = 0
        self.top = 0
        self.width = 200
        self.height = 200
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.main_widget = main_widget(self.c)
        self.setCentralWidget(self.main_widget)
        self.show()


class main_widget(QWidget):
    def __init__(self, controller):
        super().__init__()
        self.c = controller
        self.layout = QVBoxLayout()
        self.initUI()

    def initUI(self):
        self.label_url = QLabel("URL to download:")
        self.le_url = custom_line_edit()

        self.box_widget = QWidget()
        self.box_layout = QHBoxLayout()
        self.label_box = QLabel("Keep videos?")
        self.box_keep_videos = QCheckBox()
        self.box_layout.addWidget(self.label_box)
        self.box_layout.addWidget(self.box_keep_videos)
        self.box_widget.setLayout(self.box_layout)

        self.button_download = QPushButton("Download!")
        self.button_download.clicked.connect(self.c.download)
        self.info_widget = info_widget(self.c)

        self.layout.addWidget(self.label_url)
        self.layout.addWidget(self.le_url)
        self.layout.addWidget(self.box_widget)
        self.layout.addWidget(self.button_download)
        self.layout.addWidget(self.info_widget)
        self.setLayout(self.layout)


class info_widget(QWidget):
    def __init__(self, controller):
        super().__init__()
        self.c = controller
        self.layout = QVBoxLayout()
        self.initUI()

    def initUI(self):
        self.setLayout(self.layout)

    def delete_widgets(self):
        for i in reversed(range(self.layout.count())):
            self.layout.itemAt(i).widget().setParent(None)

    def set_scanning(self):
        self.delete_widgets()
        label = QLabel("Scanning!")
        self.layout.addWidget(label)
        self.setLayout(self.layout)

    def set_wrong_input(self):
        self.delete_widgets()
        label = QLabel("Not a valid URL.")
        self.layout.addWidget(label)
        self.setLayout(self.layout)

    def set_progress(self, amount):
        self.delete_widgets()
        self.progress = QProgressBar()
        self.progress.setMinimum(0)
        self.progress.setMaximum(amount)

        self.label_progress = QLabel("0 of {}".format(amount))
        self.layout.addWidget(self.progress)
        self.layout.addWidget(self.label_progress)
        self.setLayout(self.layout)

    def update_progress(self, val, amount):
        self.progress.setValue(val)
        self.label_progress.setText("{} of {}".format(val, amount))


class custom_line_edit(QLineEdit):
    """
    Custom line edit which allows the setting of an 'alarm' when the
    input is incorrect.
    """
    def __init__(self, value=None):
        super().__init__(value)
        self.alarm = False

    def activate_wrong_input(self):
        if not self.alarm:
            pal = QPalette()
            pal.setColor(self.backgroundRole(), Qt.red)
            self.setPalette(pal)
            self.alarm = True
        return

    def deactivate_wrong_input(self):
        if self.alarm:
            pal = QPalette()
            pal.setColor(self.backgroundRole(), Qt.white)
            self.setPalette(pal)
            self.alarm = False
        return


class Downloader(QThread):
    total = pyqtSignal(object)
    update = pyqtSignal(object, object)
    scanning = pyqtSignal()
    incorrect_input = pyqtSignal()
    correct_input = pyqtSignal()

    def __init__(self, url, keep_videos=False):
        super().__init__()
        import youtube_dl
        self.opts = {'outtmpl': '%(title)s.%(ext)s',
                     'postprocessors': [{
                         'key': 'FFmpegExtractAudio',
                         'preferredcodec': 'mp3'}]}
        self.ydl = youtube_dl.YoutubeDL(self.opts)
        self.url = url

    def run(self):
        try:
            self.scanning.emit()
            self.correct_input.emit()
            urls = self.to_list(self.url)
        except:
            self.incorrect_input.emit()
            return
        self.download_list(urls)

    def to_list(self, url):
        result = self.ydl.extract_info(url, download=False)

        if 'entries' in result:
            urls = []
            for video in result['entries']:
                urls.append(video['webpage_url'])
            return urls
        else:
            return [url]

    def download_list(self, urls):
        self.total.emit(len(urls))
        for index, url in enumerate(urls):
            self.ydl.extract_info(url)
            self.update.emit(index + 1, len(urls))



if __name__ == "__main__":
    c = controller()
    # d = Downloader()
    # d.download('https://www.youtube.com/watch?v=CGjoFNgVMF4')
    # # d.download('https://www.youtube.com/playlist?list=PL6ystPsdTZra49o1CCmzlnW0rrrSuIQ2C')
    # d.download('https://www.youtube.com/playlist?list=PL6ystPsdTZrZCNVg9c0T3vaHhuXNyLHGR')

